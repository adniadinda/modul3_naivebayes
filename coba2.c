#include <conio.h>
#include <stdio.h>
#include "boolean.h"
#define nMax 7

typedef struct {
	int JKel;
		/* 1 : Wanita
		   2 : Pria
		*/
	int Pend;
		/* 1 : SMA
		   2 : S1
		   3 : S2
		*/
	int Bid_Pkj;
		/* 1 : Pendidikan
		   2 : Marketing
		   3 : Wirausaha
		   4 : Profesional
		*/
	int KUsia;
		/* 1 : Muda
		   2 : Tua
		*/
	boolean KKredit;
		/* 1 : True
		   2 : False
		*/
} PKK;


int main() {
	//kamus
//	TabPKK T;
	PKK P[10];
	//algoritma
	//Data 1
	P[0].JKel = 1;
	P[0].Pend = 3;
	P[0].Bid_Pkj = 1;
	P[0].KUsia = 2;
	P[0].KKredit = true;
	
	//Data 2
	P[1].JKel = 2;
	P[1].Pend = 2;
	P[1].Bid_Pkj = 2;
	P[1].KUsia = 1;
	P[1].KKredit = true;
	
	//Data 3
	P[2].JKel = 1;
	P[2].Pend = 1;
	P[2].Bid_Pkj = 3;
	P[2].KUsia = 2;
	P[2].KKredit = true;
	
	//Data 4
	P[3].JKel = 2;
	P[3].Pend = 2;
	P[3].Bid_Pkj = 4;
	P[3].KUsia = 2;
	P[3].KKredit = true;
	
	//Data 5
	P[4].JKel = 2;
	P[4].Pend = 3;
	P[4].Bid_Pkj = 4;
	P[4].KUsia = 1;
	P[4].KKredit = false;
	
	//Data 6
	P[5].JKel = 2;
	P[5].Pend = 1;
	P[5].Bid_Pkj = 3;
	P[5].KUsia = 1;
	P[5].KKredit = false;
	
	//Data 7
	P[6].JKel = 1;
	P[6].Pend = 1;
	P[6].Bid_Pkj = 2;
	P[6].KUsia = 1;
	P[6].KKredit = false;
	
	//Data 8
	P[7].JKel = 1;
	P[7].Pend = 2;
	P[7].Bid_Pkj = 1;
	P[7].KUsia = 2;
	P[7].KKredit = false;
	

printf("|Jenis Kelamin	| Pendidikan	|	Bidang Pekerjaan	| Kelompok Usia	| Kartu Kredit	|\n");
printf("|Wanita		|S2		|Pendidikan			|Tua		|True		|\n");
printf("|Pria		|S1		|Marketing			|Muda		|True		|\n");
printf("|Wanita		|SMA		|Wirausaha			|Tua		|True		|\n");
printf("|Pria		|S1		|Profesional			|Tua		|True		|\n");
printf("|Pria		|S2		|Profesional			|Muda		|False		|\n");
printf("|Pria		|SMA		|Wirausaha			|Muda		|False		|\n");
printf("|Wanita		|SMA		|Marketing			|Muda		|False		|\n");
printf("|Wanita		|S1		|Pendidikan			|Tua		|False		|\n\n");
	
	
	//Data True
	int i;
	float jum = 0;
	float TotalTrue;
	for(i=0; i<8; i++){
		if(P[i].KKredit == 1){
			jum++;
		}
	}
	printf("Naive Bayes (True)\n");
	printf("	True			: ");
	printf("%0.2f",jum);
	
	TotalTrue = jum/8.0;
	printf("\n	Total True		: ");
	printf("%0.2f",TotalTrue);
	
	float jum1 = 0;
	float JKelTrue;
	for(i=0; i<8; i++){
		if(P[i].JKel == 1 && P[i].KKredit == true){
			jum1++;
		}
	}
	printf("\n	Jenis Kelamin		: ");
	printf("%0.2f",jum1);
	JKelTrue = (jum1/jum);
	printf("\n	Jenis Kelamin True	: ");
	printf("%0.2f",JKelTrue);
	
	float jum2 = 0;
	float PendTrue;
	for(i=0; i<8; i++){
		if(P[i].Pend == 1 && P[i].KKredit == true){
			jum2++;
		}
	}
	printf("\n	Pendidikan		: ");
	printf("%0.2f",jum2);
	PendTrue = (jum2/jum);
	printf("\n	Pendidikan True		: ");
	printf("%0.2f",PendTrue);
	
	float jum3 = 0;
	float Bid_PkjTrue;
	for(i=0; i<8; i++){
		if(P[i].Bid_Pkj == 4 && P[i].KKredit == true){
			jum3++;
		}
	}
	printf("\n	Bidang Pekerjaan	: ");
	printf("%0.2f",jum3);
	Bid_PkjTrue = (jum3/jum);
	printf("\n	Bidang Pekerjaan True	: ");
	printf("%0.2f",Bid_PkjTrue);
	
	float jum4 = 0;
	float KUsiaTrue;
	for(i=0; i<8; i++){
		if(P[i].KUsia == 2 && P[i].KKredit == true){
			jum4++;
		}
	}
	printf("\n	Kelompok Usia		: ");
	printf("%0.2f",jum4);
	KUsiaTrue = (jum4/jum);
	printf("\n	Kelompok Usia True	: ");
	printf("%0.2f",KUsiaTrue);
	
	float naivebayestrue;
	naivebayestrue = (TotalTrue*JKelTrue*PendTrue*Bid_PkjTrue*KUsiaTrue);
	printf("\n		Naive Bayes True	:");
	printf("%0.2f", naivebayestrue);
	
	
	
	//Data False
	float jum5 = 0;
	float TotalFalse;
	for(i=0; i<8; i++){
		if(P[i].KKredit == 0){
			jum5++;
		}
	}
	printf("\n\nNaive Bayes (False)");
	printf("\n	False			: ");
	printf("%0.2f",jum5);
	
	TotalFalse = jum5/8.0;
	printf("\n	Total False		: ");
	printf("%0.2f",TotalFalse);
	
	float jum6 = 0;
	float JKelFalse;
	for(i=0; i<8; i++){
		if(P[i].JKel == 1 && P[i].KKredit == false){
			jum6++;
		}
	}
	printf("\n	Jenis Kelamin		: ");
	printf("%0.2f",jum6);
	JKelFalse = (jum6/jum5);
	printf("\n	Jenis Kelamin False	: ");
	printf("%0.2f",JKelFalse);
	
	float jum7 = 0;
	float PendFalse;
	for(i=0; i<8; i++){
		if(P[i].Pend == 1 && P[i].KKredit == false){
			jum7++;
		}
	}
	printf("\n	Pendidikan		: ");
	printf("%0.2f",jum2);
	PendFalse = (jum7/jum5);
	printf("\n	Pendidikan False	: ");
	printf("%0.2f",PendFalse);
	
	float jum8 = 0;
	float Bid_PkjFalse;
	for(i=0; i<8; i++){
		if(P[i].Bid_Pkj == 4 && P[i].KKredit == false){
			jum8++;
		}
	}
	printf("\n	Bidang Pekerjaan	: ");
	printf("%0.2f",jum8);
	Bid_PkjFalse = (jum8/jum5);
	printf("\n	Bidang Pekerjaan False	: ");
	printf("%0.2f",Bid_PkjFalse);
	
	float jum9 = 0;
	float KUsiaFalse;
	for(i=0; i<8; i++){
		if(P[i].KUsia == 2 && P[i].KKredit == false){
			jum9++;
		}
	}
	printf("\n	Kelompok Usia		: ");
	printf("%0.2f",jum9);
	KUsiaFalse = (jum9/jum5);
	printf("\n	Kelompok Usia False	: ");
	printf("%0.2f",KUsiaFalse);
	
	float naivebayesfalse;
	naivebayesfalse = (TotalFalse*JKelFalse*PendFalse*Bid_PkjFalse*KUsiaFalse);
	printf("\n		Naive Bayes False	: ");
	printf("%0.2f", naivebayesfalse);


	//Hasil Naive Bayes
	float naivebayes;
	naivebayes = (naivebayestrue/(naivebayestrue+naivebayesfalse))*100;
	printf("\n\nNaive Bayes	: ");
	printf("%0.2f", naivebayes);
}
